# assess-front

#### 介绍
寻星科技前端考核项目

#### 软件架构
![image-20201009134051996](https://cdn.jsdelivr.net/gh/MW-S/MWCloudImg@main/dataimage-20201009134051996.png)

#### Vue版本要求：Vue3.0

#### 开发须知：

##### 	`由于你们学习的进度不一致，先不做统一要求，你们自行认领任务后分别在各自的文件夹下完成自己的开发。`







### **前端页面参考：**

> ####  形式一：

> > #####  分页查看：
> >
> > ![image-20201009134350038](https://cdn.jsdelivr.net/gh/MW-S/MWCloudImg@main/dataimage-20201009134350038.png)
>
> 
>
> > ##### 添加修改：
> >
> > ![image-20201009134410712](https://cdn.jsdelivr.net/gh/MW-S/MWCloudImg@main/dataimage-20201009134410712.png)



> #### 形式二：
>
> > ##### 分页查看：
> >
> > ![image-20201009134421175](https://cdn.jsdelivr.net/gh/MW-S/MWCloudImg@main/dataimage-20201009134421175.png)
>
> > ##### 添加修改页面同形式一





